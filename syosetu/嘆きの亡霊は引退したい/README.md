# novel

- title: 嘆きの亡霊は引退したい　〜最弱ハンターは英雄の夢を見る〜
- title_zh: 嘆息的亡靈想引退　〜最弱獵人做了英雄的夢〜
- author: 槻影
- illust: チーコ
- source: http://ncode.syosetu.com/n6093en/
- cover: https://images-na.ssl-images-amazon.com/images/I/81Qxf5IbaEL.jpg
- publisher: syosetu
- date: 2019-07-12T21:00:00+08:00
- status: 連載
- novel_status: 0x0300

## illusts


## publishers

- syosetu

## series

- name: 嘆きの亡霊は引退したい　〜最弱ハンターは英雄の夢を見る〜

## preface


```
世界各地に存在する宝物殿とそこに眠る特殊な力の宿る宝具。富と名誉、そして力。栄光を求め、危険を顧みず宝物殿を探索するトレジャーハンター達が大暴れする時代。
幼馴染達と共に積年の夢であるハンターとなったクライは、最初の探索で六人の中で唯一自分だけ何の才能も持っていないことに気付く。
しかし、それは冒険の始まりに過ぎなかった。
「もう無理。こんな危険な仕事やめたい。ゲロ吐きそう」
「おう、わかった。つまり俺達が強くなってお前の分まで戦えばいいんだな、いいハンデだ」
「安心してね、クライちゃん。ちゃんと私達が守ってあげるから」
「あ、ストップ。そこ踏むと塵一つ残さず消滅しますよ。気をつけて、リーダー？」
強すぎる幼馴染に守られ、後輩や他のハンターからは頼られ、目指すは英雄と強力な宝具。
果たしてクライは円満にハンターをやめる事ができるのか！？
※勘違い系コメディです。
※2019/04/27〜ComicWalkerにて、コミカライズ版が連載開始しました！
※GCノベルズより書き下ろしましましの書籍版が二巻まで発売中です。また、活動報告にて、毎週月曜日に当作品について、より楽しめる情報（書籍版情報や小ネタなど）を発信していますので、よろしければご確認くださいませ！
```

## tags

- node-novel
- R15
- syosetu
- ある意味主人公最強
- コメディ
- ダンジョン
- トレジャーハンター
- ハイファンタジー
- ハイファンタジー〔ファンタジー〕
- ファンタジー
- 冒険
- 勘違い
- 宝具
- 残酷な描写あり
- 現地人主人公
- 男主人公
- 魔法

# contribute

- Wch199
- Cocytus
- 净颇梨之镜
- 飒君CONAN
- nantanr

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n6093en

## textlayout

- allow_lf2: false

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n6093en&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n6093en/)


